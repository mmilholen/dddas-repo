
var express = require('express');
var path = require('path');
var http = require('http');
var common = require('./lib/infrastructure/config/common');
var logger = require('./lib/infrastructure/service/io/logger');

var config = common.config();


var app = express();

// all environments
app.set('port', config.port);
app.use(express.favicon());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);

// stick log4js in as the express/connect logger
app.configure(function() {
   logger.setup(app);
});


require('./routes.js').setupRoutes(app);

http.createServer(app).listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));

   if(process.getuid() == 0){
      try {
          process.setgid('nobody');
          process.setuid('nobody');
      } catch (err) {
         console.log('Error dropping privileges from root to nobody');
         process.exit(1);
      }
   }
});
