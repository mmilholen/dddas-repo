
CREATE TABLE `factdata` (
  `member_id` char(40) NOT NULL,
  `fact_id` int(10) unsigned NOT NULL,
  `fact_value` varchar(30) DEFAULT NULL,
  `origin` varchar(1) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `last_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`,`fact_id`),
  KEY `in_lud` (`last_updated_date`)
);

CREATE TABLE `user` (
  `id` char(100) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dateOfBirth` int(10) unsigned NOT NULL,
  `height` decimal(10,2) unsigned NOT NULL,
  `weight` decimal(10,2) unsigned NOT NULL,
  `admin` varchar(10) NOT NULL,
  `assessments` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

