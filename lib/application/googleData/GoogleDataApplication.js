var GoogleDataRepository = require('../../infrastructure/persistence/GoogleDataRepository.js');

function GoogleDataApplication(){

}

GoogleDataApplication.prototype.saveGoogleData = function(googleDataDto, callback) {
    GoogleDataRepository.saveGoogleData(googleDataDto, callback);
};

module.exports = GoogleDataApplication;
