/*
 * So modules in nodejs are cached on require() so there
 * is no real need to implement a singleton concept since
 * we can just load single instances within a module and
 * not duplicate objects.
 */
var config = require('../config/common.js').config(),
    mysql = require('mysql');

// here, we create a single instance of some objects and configure them

var dbConfig = Object();
dbConfig.host = config.dbhost;
dbConfig.user = config.dbuser;
dbConfig.password = config.dbpassword;
dbConfig.database = config.dbdatabase;


//exports
//module.exports.db  = new mysql.createPool(dbConfig);

