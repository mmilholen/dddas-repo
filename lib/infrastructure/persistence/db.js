var mongojs = require("mongojs"),
    config = require('../config/common.js').config(),
    connection_string = config['mongoHost'] + ':' + config['mongoPort'] + '/myapp';

exports.getDb = function() {
    return mongojs(connection_string, ['myapp']);
}
