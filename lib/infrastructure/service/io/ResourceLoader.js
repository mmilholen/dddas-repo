var fs = require('fs'),
    async = require('async'),
    config = require('../../config/common.js').config(),
    logger = require('./logger').logger('ratm.resourceLoader');

// synchronously walk a directory tree looking for files
function walk(dir){
    var results = [];
    var list = fs.readdirSync(dir);

    async.each(list, function handleFile(file, callback){
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if(stat && stat.isDirectory()){
            results = results.concat(module.exports.walk(file));
        }else{
            results.push(file);
        }

        callback();
    }, function(err) { if (err) console.log(err); });
    return(results);
}

// load up data resources like assessments, calculators and rulesets
module.exports.loadResource = function(resType){
    var resourceList = {};

    // get a list of files and work through them
    var files = walk(config['dataDir'] + '/' + resType);

    // load up calculators at startup
    async.each(files, function (entry, callback){
        // if the file doesn't end in '.js', bypass it
        if(entry.substr(entry.length - 3,entry.length) != '.js'){
            callback("not a javascript file");
            return(false);
        }

        // to a bit of nasty cleanup on the filename to
        // get just the calculator id
        var filename = entry.substring(entry.lastIndexOf('/'))
        filename = filename.substring(1, filename.length);
        filename = filename.substring(0, filename.length - 3);
        filename = filename.replace('-' + resType, '');

        // now that we know the calculator id (filename), we
        // can require up the calculator
        resourceList[filename] = require(entry);

        // show what calculator we just loaded
        logger.debug('Loaded ' + resType + ': ' + filename);
        callback();
    }, function(err) { if(err) console.log(err); });
    return(resourceList);
}