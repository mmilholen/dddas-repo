/*
 * Stub out a log handler
 */

var log4js = require('log4js');
log4js.configure({
   appenders: [
     { type: 'console' },
//     { type: 'file', filename: 'cheese.log', category: 'cheese' }
  ]
});

module.exports.setup = function(app){
   var logger = module.exports.logger('ratm');
   app.use(log4js.connectLogger(logger, { level: log4js.levels.INFO, 
                                          format: ':remote-addr - -' +
                                                  ' ":method :url HTTP/:http-version"' +
                                                  ' :status :content-length ":referrer"' +
                                                  ' ":user-agent" :response-timems'
                                        }));
}

module.exports.logger = function(ns){
   var logger = log4js.getLogger(ns);
   logger.setLevel('INFO');
   return(logger);
}

