/*
 * Here, we create some generic little utility functions
 */

var fs = require('fs');
var async = require('async');

var config = require('../../config/common.js').config();


// synchronously walk a directory tree looking for files
module.exports.walk = function(dir){
   var results = [];
   var list = fs.readdirSync(dir);

   async.each(list, function handleFile(file, callback){
      file = dir + '/' + file;
      var stat = fs.statSync(file);
      if(stat && stat.isDirectory()){
         results = results.concat(module.exports.walk(file));
      }else{
         results.push(file);
      }

      callback();
   }, function(err) { if (err) console.log(err); });
   return(results);
}

// load up data resources like assessments, calculators and rulesets
module.exports.loadResource = function(resType){
   var resourceList = {};

   // get a list of files and work through them
   var files = module.exports.walk(config['dataDir'] + '/' + resType);

   // load up calculators at startup
   async.each(files, function (entry, callback){
      // if the file doesn't end in '.js', bypass it
      if(entry.substr(entry.length - 3,entry.length) != '.js'){
         callback("not a javascript file");
         return(false);
      }

      // to a bit of nasty cleanup on the filename to
      // get just the calculator id
      var filename = entry.substring(entry.lastIndexOf('/'))
      filename = filename.substring(1, filename.length);
      filename = filename.substring(0, filename.length - 3);
      filename = filename.replace('-' + resType, '');

      // now that we know the calculator id (filename), we
      // can require up the calculator
      resourceList[filename] = require(entry);

      // show what calculator we just loaded
      console.log('Loaded ' + resType + ': ' + filename);
      callback();
   }, function(err) { if(err) console.log(err); });
   return(resourceList);
}

// simple numeric sort
module.exports.numericSort = function(a, b) {
   return(a - b);
}


// there are some wierd rules that need some dynamic operations.
// this lets us define an op and the evaluate the equation
module.exports.mathOp = function(op){
   this.operation = op;

   this.evaluate = function evaluate(param1, param2) {
      // make sure we are working with numbers
      var p1 = parseFloat(param1);
      var p2 = parseFloat(param2);

      switch(this.operation) {
         case "+":
            return(p1 + p2);
         case "-":
            return(p1 - p2);
         case "*":
            return(p1 * p2);
         case "/":
            return(p1 / p2);
         case "<":
            return(p1 < p2);
         case ">":
            return(p1 > p2);
         case "=":
            return(p2);
      }
   }
}

