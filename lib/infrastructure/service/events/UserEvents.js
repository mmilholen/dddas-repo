var config = require('../../config/common.js').config(),
    userSchema = require('../../../domain/models/user.js').schema,
    UserApplication = require('../../../application/user/UserApplication.js'),
    userApp = new UserApplication(),
    events = require('events'),
    channel = new events.EventEmitter();

channel.on('syncSSO', function(result, id){

    userApp.getUserById(id, function(data, err){

        if(err) {
            console.log('error retrieving record for ID ' + id);
        }

        if (! data) {
            data = {};
        }
        var key;
        for(key in userSchema.properties) {
            if(result.hasOwnProperty(key) && userSchema.properties.hasOwnProperty(key)) {
                data[key] = result[key];
            }
        }

        data._id = id;

        userApp.saveUser(data, function(err, doc){

            console.log('user successfully synced with sso');
        })
    });
});

channel.on('updateAssessmentList', function(userId, assessmentId){
    userApp.getUserById(userId, function(user, err){

        if(err) {
            console.log('error updating assessment list for ' + userId);
        }

        if( user.assessments && user.assessments.indexOf(assessmentId) == -1 ) {
            user.assessments.push(assessmentId);
        }

        if(user.assessments == null || user.assessments == undefined) {
            user.assessments = [ assessmentId ];
        }

        userApp.saveUser(user, function(err){
            console.log("user saved");
        });

    });
});

module.exports.UserEvents = channel;