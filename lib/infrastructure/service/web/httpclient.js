var http = require("http");
var https = require("https");
var querystring = require('querystring');

exports.getJSON = function(options, onResult){
    // set some defaults
    var prot = options.port == 443 ? https : http;
    var myTimeout = options.timeout ? options.timeout : 30000;
    var postData = '';
    var contentLength;

    // if a post body is passed in, setup the headers
    if(options.postdata){
       postData = querystring.stringify(options.postdata);
       contentLength = Buffer.byteLength(postData);
       if(!options.headers){
          options.headers = {};
       }
       options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
       options.headers['Content-Length'] = contentLength;
       delete(options.postdata);
    }

    // make a request
    var req = prot.request(options, function(res){
        var output = '';
        res.setEncoding('utf8');

        // build up the body of the response
        res.on('data', function (chunk) {
            output += chunk;
        });

        // at the end of the response, call the callback
        res.on('end', function() {
            var obj = JSON.parse(output);
            onResult(res.statusCode, obj);
        });
    });

    // set a timeout on the socket
    req.on('socket', function (socket) {
        socket.setTimeout(myTimeout);
        socket.on('timeout', function() {
            console.log('timeout');
            req.abort();
        });
    });

    // deal with socket errors
    req.on('error', function(err) {
        console.log('error in getJSON: ' + err.message);
        onResult(0, err.message);
    });

    // if a post body is set, send the data
    if(postData){
       req.write(postData);
    }

    // finish off the request
    req.end();
};
