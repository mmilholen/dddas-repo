var fs = require('fs');
var env;

if(fs.existsSync('/etc/config/dddas-repo/env.json')){
   env = require('/etc/config/dddas-repo/env.json');
}else{
   env = require('../../../env.json');
}

exports.config = function() {
    return env;
};
