function AbstractController() {
    this.payload = {
        result : "",
        data : ""
    }
}

AbstractController.prototype.success = function(res, data){
    this.payload.result = "SUCCESS";
    this.payload.data = data;
    res.send(this.payload);
};

AbstractController.prototype.error = function(res, data, errorCode) {
    this.payload.result = "FAILURE";
    this.payload.errorCode = errorCode;
    this.payload.data = data;
    res.send(this.payload);
};

module.exports = AbstractController;