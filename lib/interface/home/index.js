
var express = require("express");
var path = require('path');
var ssofilter = require('../../infrastructure/security/ssofilter.js');

var app = module.exports = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/', ssofilter.doFilter(), homePage);

function homePage(req, res) {
    res.render('index', { title: 'Express' });
}