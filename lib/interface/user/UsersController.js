var UserApplicationClass = require("../../application/user/UserApplication.js"),
    UserApplication = new UserApplicationClass(),
    AbstractController = require("../AbstractController"),
    util = require('util');

function UsersController() {
    AbstractController.call(this);
}

util.inherits(UsersController, AbstractController);

UsersController.prototype.getUser = function(req, res) {

    //grab a reference to the enclosing object to use inside the callback
    var self = this;
    UserApplication.getUserById(req.params.member_id, function(user, err){
        console.log(user);

        if(err) self.error(res, "server error", 500);

        if(user) {
            self.success(res, user);
        } else {
            self.error(res, "access denied", 403);
        }

    });


};


module.exports = UsersController;

