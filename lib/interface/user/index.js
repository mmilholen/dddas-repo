var express = require('express'),
    app = module.exports = express(),
    assessmentsControllerClass = require('../assessment/AssessmentsController'),
    AssessmentsController = new assessmentsControllerClass(),
    usersControllerClass = require('./UsersController'),
    UsersController = new usersControllerClass();


//User Endpoint

app.get('/:member_id', function(req, res) {
    UsersController.getUser(req, res);
});

//Assessment endpoints under /user/

// user/{user-id}/assessments
app.get('/:member_id/assessments', function(req, res) {
    AssessmentsController.assessmentListForUser(req, res);
});

// user/{user-id}/assessments/{assessment-id}
app.get('/:member_id/assessments/:assessment_id', function(req, res) {
    AssessmentsController.assessmentPage(req, res);
});

// user/{user-id}/assessments/{assessment-id}/{page-id}
app.get('/:member_id/assessments/:assessment_id/:page_id', function(req, res) {
    AssessmentsController.assessmentPage(req, res);
});

//POST user/{user-id}/assessments/{assessment-id}/{page-id}
app.post('/:member_id/assessments/:assessment_id/:page_id', function(req, res) {
    AssessmentsController.assessmentUpdatePage(req, res);
});

