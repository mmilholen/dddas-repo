var express = require('express'),
    app = module.exports = express(),
    googleDataControllerClass = require('./GoogleDataController'),
    GoogleDataController = new googleDataControllerClass();

// Google Data Endpoint

// PUT googleData
app.put('/', function(req, res) {
    GoogleDataController.saveGoogleData(req, res);
});
