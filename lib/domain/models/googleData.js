function GoogleData(dataString) {
    this.dataString;
}

module.exports = GoogleData;
module.exports.schema = {
    "id": "/GoogleData",
    "type": "object",
    "properties": {
        "dataString" : { "type" : "string", "required": true}
    }
};
